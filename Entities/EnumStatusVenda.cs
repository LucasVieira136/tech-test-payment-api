namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        EnviadoParaTranspotadora = 2,
        Entregue = 3,
        Cancelada = 4
    }
}