using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public string Vendedor { get; set; }
        public string IdVendedor { get; set; }
        public string CpfVendedor { get; set; }
        public string EMail { get; set; }
        public string Telefone { get; set; }
        public DateTime Data { get; set; }
        public string ItensVendidos { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}