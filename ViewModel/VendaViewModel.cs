using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.ViewModel
{
    public class VendaViewModel
    {
        public string Vendedor { get; set; }
        public string IdVendedor { get; set; }
        public string CpfVendedor { get; set; }
        public string EMail { get; set; }
        public string Telefone { get; set; }
        public DateTime Data { get; set; }
        public List<string> ListaItensVendidos { get; set; }
    }
}