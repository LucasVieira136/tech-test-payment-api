using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.ViewModel;

namespace tech_test_payment_api.Controllers
{   [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendasContext _context;
        public  VendaController(VendasContext context)
        {
            _context = context; 
        }
         [HttpGet("/BuscarVendaPorId/{id:int}")]
        public IActionResult BuscarVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null)
                return NotFound();

            return Ok(venda);
        }
        [HttpPost]
        public IActionResult RegistrarVenda(VendaViewModel vm)
        {
            Venda venda = new Venda();

            if(vm.ListaItensVendidos == null)
                return BadRequest();

            venda.Vendedor = vm.Vendedor;
            venda.IdVendedor = vm.IdVendedor;
            venda.CpfVendedor = vm.CpfVendedor;
            venda.EMail = vm.EMail;
            venda.Telefone = vm.Telefone;
            venda.Data = vm.Data;

            venda.ItensVendidos = vm.ListaItensVendidos.Aggregate((x, y) => x + "; " + y);
            venda.Status = (EnumStatusVenda)0;
            
            _context.Add(venda);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarVendaPorId), new { id = venda.Id}, venda);
        }
        [HttpPut("/Atualizar/")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id); 

            if(vendaBanco == null)
                return NotFound();

            if(vendaBanco.Status == (EnumStatusVenda)0 && (venda.Status == (EnumStatusVenda)1 || venda.Status == (EnumStatusVenda)4))
            {
                vendaBanco.Status = venda.Status;
            }
            else if(vendaBanco.Status == (EnumStatusVenda)1 && (venda.Status == (EnumStatusVenda)2 || venda.Status == (EnumStatusVenda)4))
            {
                vendaBanco.Status = venda.Status;
            }
            else if(vendaBanco.Status == (EnumStatusVenda)2 && venda.Status == (EnumStatusVenda)3)
            {
                vendaBanco.Status = venda.Status;
            }
            else
            {
                return BadRequest();
            }

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }
    }
}